<?php
    include 'Model/loginModel.php'; // Ici on récupère les variables défini dans le loginModel

    if(isset($_POST['submit'])){ //Si la variable submit existe, créée en cliquant sur le bouton
        
        if (!empty($_POST['pseudo']) && !empty($_POST['pass'])){ //On verifie que les champs pseudo et pass ne sont vides
            
            $req = $bdd->query('SELECT id, pseudo, pass FROM membres WHERE pseudo=\'' . $_POST['pseudo'] . '\''); // On execute la requête SQL

            while ($rs = $req->fetch()){ // On récupère ligne par ligne jusqu'à ne plus en avoir
                
                // Pour chaque ligne ------------
                
                if (($_POST['pseudo'] == $rs['pseudo']) && password_verify($_POST['pass'],  $rs['pass'])){ // On verifie que le pseudo tapé est le même que celui de la BDD
                    
                    session_start(); // On démarre les sessions
                    $_SESSION['id'] = $rs['id']; // On initialise la session 'id' avec l'id de la BDD
                    $_SESSION['login'] = $rs['pseudo']; // On initialise la session 'login' avec le pseudo de la BDD
                    $_SESSION['time'] = time(); // Initialisation de la session time au temps de création
                    header('Location: index.php'); // On renvoi vers la page index.php
                    echo 'bogoss';
                    
                }else{
                    echo 'L\'utilisateur n\'est pas inscrit';
                }
            }

        }else{
           echo 'Veuillez remplir tout les champs';
        }
        
    }
    

    include 'View/loginView.php'; // Si 'submit" n'existe pas, on envoi sur la vue Login
    
?>