<?php
	if (isset($_POST['logout'])){ // Si le bouton logout est pressé
		session_destroy(); // On détruit les SESSIONS
		header('Location: login.php'); // On redirige vers login.php
	}
?>