<?php
    session_start(); //Démarrage des sessions
    if ((isset($_SESSION['login']))){ // On ne peut être sur index.php que si la SESSION login existe

      // On créer un cookie nommé pseudo, avec comme valeur la SESSION login, et une durée de 1800s soit 30min
      setcookie('pseudo', $_SESSION['login'], time()+1800);
    }else{
      header("Location: login.php");
    }
    
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css"
    href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
  <link rel="stylesheet" href="css/style.css"/>
  <title>Accueil</title>
</head>

<body>
<header>
<?php include 'View/navbarLog.php'; ?>
</header>
<main class="container">
<?php 
  $tmp=time() - $_SESSION['time']; // On définit la variable tmp, ce qui nous donne la durée en s depuis la connexion

  if ($tmp < 1800) {
    // Si l'utilisateur est connecté depuis moins de 30min

    // Alors on affiche les données
    echo('<h1>Bienvenue sur la page d\'accueil </h4>');
    echo('<h1>Bonjour '.ucwords($_SESSION['login']).'</h1>');
    echo('<h2>Ton ID est : '.$_SESSION['id'].'</h2>');
    echo('<h2>Ton cookie est : '.ucwords($_COOKIE['pseudo']).'</h2>');
    echo('<h3>En ligne depuis '.$tmp.'s<h3>');

    // Le bouton redirige vers logout.php qui tue les SESSIONS et déconnecte l'utilisateur
    echo('<form method="POST" action="logout.php">
    <button class="btn btn-outline-danger button" name="logout" type="submit"><span>Se déconnecter</span></button> </form>');
  }else{
    session_destroy();
    header('Location: login.php');
  }
?>
</main>
  <!-- Correspond a la modal en bas de page pour l'acceptation des cookies -->
  <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script> 
  <script>
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#237afc"
        },
        "button": {
          "background": "#fff",
          "text": "#237afc"
        }
      },
      "theme": "classic",
      "type": "opt-out",
      "content": {
        "message": "En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies ou autres traceurs.",
        "allow": "J'ai compris",
        "deny": "Je n'accepte pas",
        "link": "En savoir plus",
        "href": "cookie.php"
      }
    });
  </script>
</body>

</html>
