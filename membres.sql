-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 29 nov. 2019 à 13:51
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ppe`
--

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

DROP TABLE IF EXISTS `membres`;
CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_inscription` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `membres`
--

INSERT INTO `membres` (`id`, `pseudo`, `pass`, `email`, `date_inscription`) VALUES
(1, 'guillaume', '$2y$10$zbNKYUGs55ek1l5uQXcgxuEPU4LAq48J9/CynyMDAe4vhMLzuovUW', 'guillaume.d@gmail.com', '2019-11-29'),
(2, 'jojolefou', '$2y$10$5pzvMhArbAdUZsBKhFZvmOu40qqGYrDfJ7BTS11hwKhWXItViGpWi', 'johann.pgm@ps4.fr', '2019-11-29'),
(3, 'quentinLeFou', '$2y$10$70ZO1rtIrwepIuJ.sSavv./H7.pqRJ5sxgoAnpc8tCUjoTLRNZagu', 'quentin', '2019-11-29'),
(4, 'maloDrift', '$2y$10$ZOeqeBS8GdOhabwaqz4ZVeNf9pQuaUlvOUm4PjBEcEqHm0DQP0kw.', 'malo@twingo.fr', '2019-11-29');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
