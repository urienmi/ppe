<?php
    
    try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=ppe;charset=utf8;', 'root', ''); // Connexion à la BDD
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Affichage des erreurs
    }
    catch (exception $e)
    {
        die('Erreur : '. $e->getMessage()); // Si il y a une erreur alors on affiche le message
    }
?>