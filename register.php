<?php
     //On affiche la view de l'inscription
    include 'Model/bdd.php'; //On récupère les données pour ce connecter à la BDD
    include 'Model/registerModel.php';
    if (isset($_POST['submit'])){ //Si la valeur de submit existe
        if (!empty($_POST['pseudo']) && !empty($_POST['email']) && !empty($_POST['pass']) && !empty($_POST['confPass'])){
            $pseudo = $_POST['pseudo']; //On récupère le pseudo
            $email = $_POST['email']; //On récupère l'email
            $confPass = $_POST['confPass']; //On récupère la confirmation du mot de passe
            
            

            // Vérification de la validité des informations
            
            if ($_POST['pass'] == $confPass){ //On verifie que les 2 mots de passe sont identiques
                
                $mdp = $_POST['pass'];
                if(conforme($mdp)){
                    // Hachage du mot de passe
                    $pass_hache = password_hash($_POST['pass'], PASSWORD_DEFAULT);
                    
                    // Insertion
                    $req = $bdd->prepare('INSERT INTO membres(pseudo, pass, email, date_inscription) VALUES(:pseudo, :pass, :email, CURDATE())');
                    $req->execute(array(
                    'pseudo' => $_POST['pseudo'],
                    'pass' => $pass_hache,
                    'email' => $_POST['email']));

                    session_start();
                    $_SESSION['login'] = $pseudo;
                    $_SESSION['mail'] = $email;
                    $_SESSION['time'] = time();

                    header("Location: index.php"); //On redirige vers la page d'acceuil
                }else{
                    echo '<h4>Mot de passe non conforme</h4>';
                    echo '<ul><li>Il doit être composé de minimum 6 caractères</li>';
                    echo '<li>Il doit comporter au moins une majuscule</li>';
                    echo '<li>Il doit comporter au moins une minuscule</li>';
                    echo '<li>Il doit comporter au moins un caractère spécial</li></ul>';
                }

        }else{ //Sinon on prévient l'utilisateur
            echo '<h4>Mots de passe différents</h4>';
        }
    }else{
        echo 'Veuillez remplir tout les champs';
    }
    
       
    } 
    
    

    include 'View/registerView.php';

?>
