<!DOCTYPE html>
<html lang="fr">

<head>

    <title>Se connecter</title>
    <link rel="stylesheet" href="css/style.css" />
</head>

<body>
    <header>
        <?php 
        if(isset($erreur)) { //Si une erreur existe, alors on l'affiche
            echo $erreur;
        }
    ?>
        <form method="POST">
            <div class="inputs">
                <label>Nom d'utilisateur : </label>
              
                <input type="text" placeholder="Entrez votre pseudo" id="login" name="pseudo">
               
            </div>
            <br>
            <div class="inputs">
                <label>Mot de passe : </label>
              
                <input type="password" placeholder="Entrez votre mot de passe" name="pass">
                <br>
            </div>
            <div class="buttons">
                <button class="button" type="submit" name="submit"><span>Se connecter</span></button>
                <!-- Ce bouton permet de créer submit grace à son name -->
        </form>
        <a name="toRegister" href="register.php"><button class="button"><span>Créer un compte</span></button></a>
        </div>
    </header>
</body>

</html>