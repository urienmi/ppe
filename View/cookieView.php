<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://bootswatch.com/4/pulse/_bootswatch.scss">
    <link rel="stylesheet" href="https://bootswatch.com/4/pulse/_variables.scss">
    <link rel="stylesheet" href="css/style.css"/>
    <title>Utilisation de cookies</title>
</head>
<body>

<header>
<?php include 'View/navbarLog.php'; ?>
</header>
<main class="container my-5">
    <h1 class="mb-5">Réglementation des cookies</h1>
    
    <h2 class="mb-5">Lorsque vous naviguez sur notre site internet, des informations sont susceptibles 
    d'être enregistrées, ou lues, dans votre terminal, sous réserve de vos choix.</h2>
    
    <div class="mb-5">
        <h4>Acceptez-vous le dépôt et la lecture de cookies afin que nous et nos partenaires 
        puissions analyser vos centres d'intérêts pour vous proposer des publicités personnalisées ?</h4>
        <form class="pl-2" action="../cookie.php" method="post">
            <div class="row p-2">
                <input class="m-1" type="radio" name="yes1" id="customRadio1" value="Oui"><h5>Oui</h5>
            </div>
            <div class="row p-2">
                <input class="m-1" type="radio" name="no1" id="customRadio1" value="Non"><h5>Non</h5>
            </div>
        </form>
    </div>
    
    <div class="mb-5">
        <h4>Acceptez-vous le dépôt et la lecture de cookies afin d'analyser votre navigation et nous 
        permettre de mesurer l'audience de notre site internet :</h4>
        <form class="pl-2" action="../cookie.php" method="post">
            <div class="row p-2">
                <input class="m-1" type="radio" name="yes1" id="yes1" value="Oui"><h5>Oui</h5>
            </div>
            <div class="row p-2">
                <input class="m-1" type="radio" name="no1" id="no1" value="Non"><h5>Non</h5>
            </div>
        </form>
    </div>
    

    <div class="mb-5">
        <h4>Acceptez-vous le dépôt et/ou la lecture de cookies pour vous permettre de partager des contenus de notre site avec d'autres personnes ou de faire connaître à ces autres personnes votre 
        consultation ou votre opinion  (boutons "J'aime" de Facebook par exemple) :</h4>
        <form class="pl-2" action="../cookie.php" method="post">
            <div class="row row p-2">
                <input class="m-1" type="radio" name="yes1" id="yes1" value="Oui"><h5>Oui</h5>
            </div>
            <div class="row p-2">
                <input class="m-1" type="radio" name="no1" id="no1" value="Non"><h5>Non</h5>
            </div>
        </form>
    </div>
    
</main>
<!-- Script du JS Bootstrap -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>